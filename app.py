from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy 
from flask_marshmallow import Marshmallow 
from flask_cors import CORS, cross_origin
import os
import random
import urllib.request
import requests
import base64
import json
import logging

app = Flask(__name__)
CORS(app, support_credentials=True)


@cross_origin(supports_credentials=True)
@app.route('/NumberPlateDetectionEP', methods=['GET','POST'])
def get():

    urlFB=[]
    app.logger.info('this is an INFO message')
    print('Beginning file download with urllib2...')

    #word_site = "http://svnweb.freebsd.org/csrg/share/dict/words?view=co&content-type=text/plain"

    #response = urllib.request.urlopen(word_site)
    #txt = response.read().decode("utf-8") 
    #WORDS = txt.splitlines()
    
    WORDS=["Bruckner","Davis","Harriman","turbulent","haploidy","housemate","Sheridan","profusion","lap","girt","pyrite","shagging","lizard","layup","roadrunner","ipsilateral","deposition","agony","hesitater"]
    

    #WORDS1=WORDS[:200]

    word=random.choice(WORDS)

    fileName= word+'.png'


    if request.method == 'POST':
        data = request.form
        jsn=request.get_json()
        #url=jsn['url']
        #urlFBe=urlFB.append(data['url'])  
        #return jsonify({'msg':'recieved','urlh':urlFBe})

        #IMAGE_PATH=data['url']

        IMAGE_PATH	=jsn['url']


        URL=str(IMAGE_PATH)
        urllib.request.urlretrieve(URL, fileName)
        #print(os.path.basename(image.png))
        imageFile=str(fileName)


    #if request.method == 'GET':

        #IMAGE_PATH = '/home/bspwm/Pictures/images (1).jpeg'
        SECRET_KEY = 'sk_95b80c920608409e71360452'
       

        with open(imageFile, 'rb') as image_file:
            img_base64 = base64.b64encode(image_file.read())

        url = 'https://api.openalpr.com/v2/recognize_bytes?recognize_vehicle=1&country=ind&secret_key=%s' % (SECRET_KEY)
        r = requests.post(url, data = img_base64)

        #jsn=json.dumps(r.json(), indent=4,sort_keys=True)
        #print(r.json())
        plateNo=""
        data1=r.json()
        CreditsUsed=data1["credits_monthly_used"]
        for dat in data1['results']:
            plateNo=dat['plate']
        if os.path.exists(fileName):
			
        	os.remove(fileName)
        else:
        	print("the file does not exist")
        return jsonify({'NumberPlate Detected':plateNo,'CreditsUsage':CreditsUsed})
        #return jsonify({'msg':'success'})
    #return jsn





# Run Server
if __name__ == '__main__':
  app.run()#debug=True
